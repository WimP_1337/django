### 1. Создайте папку для проекта

### 2. Склонируйте в нее репозиторий

`git clone https://gitlab.com/WimP_1337/django.git`

### 3. Создайте виртуальное окружение virtualenv

### 4. Активируйте виртуальное окружение

`source /bin/activate`

### 5. Установка зависимостей

`pip3 install -r requirements.txt`



### 6. Перейдите в рабочую директорию проекта

`/django_project/djangosite/testsite`

### 7. Запуск сервера
`python3 manage.py runserver`

По адресу 127.0.0.1:8000/start_docker будет доступен функционал проекта

### 8. Snort

За основу взят docker image linton/snort-base

Для установки:

`docker pull linton/snort-base`

Запуск:

`docker run -it --rm --net=host linton/snort-base /bin/bash`
