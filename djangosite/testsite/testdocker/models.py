from django.db import models


class Document(models.Model):
    document = models.FileField(upload_to='volume/traffic')
    # rule = models.FileField(upload_to='volume/rules')


class Rules(models.Model):
    filename = models.CharField(max_length=100, blank=True)
    rule_file = models.FileField(upload_to='volume/rules', blank=True, null=True)
    text_rule = models.TextField(max_length=1000, blank=True)
    description = models.CharField(max_length=100, blank=True)


class Archive(models.Model):
    date = models.DateField(auto_now=True)
    rule_file = models.CharField(max_length=100, blank=True)
    traffic_file = models.CharField(max_length=100, blank=True)
    alerts = models.IntegerField(default=0, blank=True)
