from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', index, name="home"),
    path('start_docker/', start_docker),
    path('add_rule/', add_rule),
    path('show_result/', show_result),
    path('rules/', rules),
    path('results_archive/', results_archive),
    path('results_archive/<slug:arch_slug>/', show_archive_result)
]
