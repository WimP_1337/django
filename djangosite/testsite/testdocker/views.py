import shutil

from django.http import HttpResponseNotFound
from django.shortcuts import render

from .forms import *
from .models import Rules
from .models import Archive
import shutil
import time


# Create your views here.

def index(request):
    status = ''
    name = ''
    if request.method == 'POST':
        form = NameForm(request.POST)

        if form.is_valid():
            res = os.system(f"docker run -i --name {form.cleaned_data['docker_name']} alpine:3.14 /bin/sh")
            if res != 32000:
                name = f"Запущен docker контейнер с именем {form.cleaned_data['docker_name']}"
                status = "OK"
            else:
                status = "Failed"

    else:
        form = NameForm()

    return render(request, 'testdocker/index.html',
                  {'form': form, 'status': status, 'name': name, 'my_list': [1, 2, 3]})


def start_docker(request):
    status = ''
    name = ''
    show_result = 0
    volume = {
        os.path.abspath('volume'): {'bind': '/mnt/volume',
                                    'mode': 'rw'}
    }

    rules_list = os.listdir('volume/rules')
    rules = [rule for rule in rules_list]

    if request.POST:

        # delete previous files
        prev_files = os.listdir('volume/logs')
        for file in prev_files:
            os.remove(f'volume/logs/{file}')

        # удаление файла с трафиком
        prev_files = os.listdir('volume/traffic')
        for file in prev_files:
            os.remove(f'volume/traffic/{file}')

        load_traffic_form = LoadTrafficForm(request.POST, request.FILES)
        docker_form = DockerForm(request.POST)
        print(request.POST)
        if load_traffic_form.is_valid() and docker_form.is_valid():
            # print(form.cleaned_data['docker_image'])
            load_traffic_form.save()

            client = docker.from_env()
            if docker_form.cleaned_data['docker_image'] == 'linton/snort-base:latest':
                traffic_struct = detect_traffic('./volume')
                traffic_file = traffic_struct[0]
                rule_path = './rules/' + request.POST["rules"]
                print(traffic_file, rule_path)

                container = client.containers.run(docker_form.cleaned_data['docker_image'],
                                                  # '/bin/bash',
                                                  f'snort -r {traffic_file} -c {rule_path} -l logs',
                                                  volumes=volume, detach=True, tty=True,
                                                  stdin_open=True, stdout=True, stream=True,
                                                  auto_remove=True, working_dir='/mnt/volume')

                show_result = 1

                start_time = int(time.time())
                while True:
                    if os.listdir('volume/logs/'):
                        time.sleep(2)
                        shutil.copyfile('volume/logs/alert',
                                        f'results/{str(int(time.time())) + "_" + request.POST["rules"] + "_" + traffic_struct[1]}')
                        break
                    elif int(time.time()) - start_time > 5:
                        status = "Failed"
                        break
                with open('volume/logs/alert', 'r') as fp:
                    analysis = fp.read()

                archive = Archive(rule_file=request.POST["rules"], traffic_file=traffic_struct[1],
                                  alerts=int(len(re.findall(r'\[\*\*\]', analysis, re.I | re.M)) / 2))
                archive.save()
            else:
                container = client.containers.run(docker_form.cleaned_data['docker_image'], '/bin/sh', volumes=volume,
                                                  tty=True, detach=True, stdin_open=True, stdout=True,
                                                  auto_remove=True)
            name = container.name
            status = "OK"

    else:
        load_traffic_form = LoadTrafficForm()
        docker_form = DockerForm()
    return render(request, 'testdocker/start_docker.html',
                  {'load_traffic_form': load_traffic_form, 'docker_form': docker_form, 'status': status, 'name': name,
                   'show_result': show_result, 'rules': rules})


def add_rule(request):
    keyboard_input = False
    if request.POST:
        keyboard_input = request.POST['is_keyboard_input'] if 'is_keyboard_input' in request.POST else False
        form = AddRuleForm(request.POST, request.FILES)
        print(request.POST)
        if form.is_valid():
            if "filename" in request.POST:
                if request.POST["filename"] != '':
                    with open(f'volume/rules/{request.POST["filename"]}.rules', 'w', encoding='utf-8') as rule_file:
                        rule_file.write(request.POST['text_rule'])
            form.save()
    else:
        form = AddRuleForm()
    return render(request, 'testdocker/add_rule.html', {'form': form, 'keyboard_input': keyboard_input})


def detect_traffic(listdir):
    traffic = os.listdir(listdir + '/traffic')[0]

    return './traffic/' + traffic, traffic


def show_result(request):
    with open('volume/logs/alert', 'r', encoding='utf-8') as file:
        analyzed_traffic = file.read()
    return render(request, 'testdocker/show_result.html', {'analyzed_traffic': analyzed_traffic})


def rules(request):
    rules = Rules.objects.all()
    rules_db = []

    for rule in rules:
        tmp = []
        tmp.append(rule.filename)
        tmp.append(rule.rule_file)
        tmp.append(rule.text_rule)
        tmp.append(rule.description)
        rules_db.append(tmp)

    print(rules_db)
    return render(request, 'testdocker/rules.html', {'rules_db': rules_db})


def results_archive(request):
    files = os.listdir('results')
    slugs = []
    for file in files:
        slugs.append(int(file[:10]))
    slugs.sort()
    results = Archive.objects.all()
    results_db = []
    for index, res in enumerate(results):
        tmp = []
        tmp.append(res.date)
        tmp.append(res.rule_file)
        tmp.append(res.traffic_file)
        tmp.append(res.alerts)
        tmp.append(slugs[index])
        results_db.append(tmp)
    print(slugs)
    return render(request, 'testdocker/results_archive.html', {'slugs': slugs, 'results_db': results_db})


def show_archive_result(request, arch_slug):
    files = os.listdir('results')
    for file in files:
        if arch_slug in file:
            with open(f'results/{file}', 'r', encoding='utf-8') as file:
                analyzed_traffic = file.read()

    return render(request, 'testdocker/show_result.html', {'analyzed_traffic': analyzed_traffic})


def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')
