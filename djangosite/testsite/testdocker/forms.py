import os

from django import forms
import docker
import re
from .models import Document
from .models import Rules


# testing form
class NameForm(forms.Form):
    docker_name = forms.CharField(label='Docker Name', max_length=100)


# form to load traffic to volume/traffic to next analysis
class LoadTrafficForm(forms.Form, forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(LoadTrafficForm, self).__init__(*args, **kwargs)
        self.fields['document'].label = "Выберите файл для анализа"
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'custom-file-upload'
            self.fields[field].widget.attrs['data-btn-text'] = 'Выберите файл'

    class Meta:
        model = Document
        fields = ('document',)

    # # выбор правила
    # rules_list = os.listdir('volume/rules')
    # rules = []
    # for rule in rules_list:
    #     rules_tuple = (rule, rule)
    #     rules.append(rules_tuple)
    # choice_rule = forms.ChoiceField(choices=rules, label='Choose a snort rule', required=False)


# form to choice docker image to run
class DockerForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(DockerForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control pointer required'

    # выбор докер образов
    client = docker.from_env()
    image_list = client.images.list()
    images = []
    for image in image_list:
        re_images = re.findall(r'\'(.*?)\'', str(image))
        for re_image in re_images:
            image_tuple = (re_image, re_image)
            images.append(image_tuple)
    docker_image = forms.ChoiceField(choices=images, label='Выберите IDS', required=False)


# form to load rules to DB
class AddRuleForm(forms.Form, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddRuleForm, self).__init__(*args, **kwargs)

        # Redefining of attrs
        self.fields['rule_file'].widget.attrs['class'] = 'custom-file-upload'
        self.fields['rule_file'].widget.attrs['data-btn-text'] = 'Выберите файл'

        self.fields['description'].widget.attrs['class'] = 'form-control'
        self.fields['description'].widget.attrs['placeholder'] = 'Описание'

        self.fields['filename'].widget.attrs['class'] = 'form-control'
        self.fields['filename'].widget.attrs['placeholder'] = 'Имя файла'

        self.fields['text_rule'].widget.attrs['class'] = 'form-control word-count'
        self.fields['text_rule'].widget.attrs['placeholder'] = 'Введите правило...'
        self.fields['text_rule'].widget.attrs['rows'] = '5'
        self.fields['text_rule'].widget.attrs['data-maxlength'] = '1000'
        self.fields['text_rule'].widget.attrs['data-info'] = 'textarea-words-info'

    class Meta:
        model = Rules
        fields = ('filename', 'rule_file', 'text_rule', 'description',)
